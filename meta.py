#! /bin/python
import os
from PIL import Image
import time

def getfiles():
	'''Получение списка файла в директории'''
	return os.listdir(".")

def sortfiles(files):
	'''Скриншоты делаются в этих форматах'''
	return [p for p in files if p.endswith(".png") or p.endswith(".jpg") ]

    
def mycrop(file, rash):
	'''Обрезание объекта PIL по координатам'''
	return file.crop(rash)

def rename(name):
	'''Переименование по формату даты'''
	sec = os.path.getmtime(name)
	gmtime  = time.gmtime(sec)
	string_name = time.strftime("%Y-%m-%d_%b-%H:%M:%S.png", gmtime)
	return string_name

def crop_and_save(name, rash):
	root_file = Image.open(name)
	newfile = mycrop(root_file, rash)
	new_name = rename(name)
	os.rename(name, "old/{}".format(name))
	newfile.save(new_name)
	os.rename(new_name, "pic/{}".format(new_name))

def check():
	if "pic" not in FILES:
		os.mkdir("pic")
	if "old" not in FILES:
		os.mkdir("old")


FILES = getfiles()
pngfiles = sortfiles(FILES)

check()

rash = {"1": [240, 0, 1680, 1080], "2": [0, 0, 1920, 1080], 
		"3": [1920+240, 0, 1920*2-240, 1080], "4": [1920, 0, 1920*2, 1080],
		"5": [0, 60, 1920, 960]}
print('''
1. 1440 left
2. 1920 left
3. 1440 right
4. 1920 right
5. x*960 right''')
s = str(input())
px = tuple(rash[s])

for i in pngfiles:
	crop_and_save(i, px)
